import React, { useState } from "react";
import {
  Grid,
  Hidden,
  Card,
  CardHeader,
  CardContent,
  TextField,
  Button
} from "@material-ui/core";

function App() {
  const [reoderredArray, setReoderredArray] = useState<number[]>([]);
  const [inputString, setInputString] = useState("");
  const [inputErrMsg, setInputErrMsg] = useState<string>();

  /**Recursive function Option 1: Do exactly and return same result with the example */
  const fnReorder = (
    arr: number[],
    ovenResultArr: number[] = [],
    oddResultArr: number[] = []
  ): number[] => {
    if (ovenResultArr.length > 0 && oddResultArr.length > 0) {
      return [...ovenResultArr, ...oddResultArr];
    } else {
      return fnReorder(
        arr,
        arr.filter((item, i) => i % 2 === 0),
        arr.filter((item, i) => i % 2 !== 0)
      );
    }
  };
  /**Recursive function Option 2: input [1,2,3,4,5,6,7,8,9,10] will return [1,3,5,7,9,10,8,6,4,2] */
  // const fnReorder = (iArr: number[], arrIndex: number = 0): number[] => {
  //   if (arrIndex < iArr.length) {
  //     return arrIndex % 2 === 0
  //       ? [iArr[arrIndex], ...fnReorder(iArr, arrIndex + 1)]
  //       : [...fnReorder(iArr, arrIndex + 1), iArr[arrIndex]];
  //   } else return [];
  // };

  const fnValidateInput = (arr: number[]): boolean => {
    if (arr.includes(NaN)) {
      setInputErrMsg('Input cannot be empty and must be a number array splited by ","');
      return false;
    } else if (arr.length < 2) {
      setInputErrMsg("Input array is not long enough");
      return false;
    }
    setInputErrMsg(undefined);
    return true;
  };

  const fnSubmit = () => {
    /**Convert string input to array */
    const parsedArr = inputString
      .split(",")
      .map(num => (num.length > 0 ? +num : NaN));
    /**Validate input */
    if (!fnValidateInput(parsedArr)) return;
    /**Call Recursive function, receive result and show to user */
    setReoderredArray(fnReorder(parsedArr));
  };

  return (
    <Grid container>
      <Hidden smDown>
        <Grid item md={3}></Grid>
      </Hidden>
      <Grid item xs={12} md={6}>
        <Card>
          <CardHeader title={`Reorder Array: [${reoderredArray}]`}></CardHeader>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  title="Input Array:"
                  fullWidth
                  variant="outlined"
                  error={inputErrMsg ? true : false}
                  helperText={inputErrMsg}
                  value={inputString}
                  onChange={e => {
                    setInputString(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Button variant="outlined" type="button" onClick={fnSubmit}>
                  Submit
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

export default App;
